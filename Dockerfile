From maven:3.6.3-openjdk-11
ARG code_version
ENV abc=$code_version
LABEL maintainer "diwakar777.up@gmail.com"
RUN mkdir /dir01 && useradd -ms /bin/sh appuser
COPY [ "target/assignment-$code_version.jar","entrypoint.sh","/dir01/" ]
USER appuser
WORKDIR "/dir01/"
EXPOSE 8090
ENTRYPOINT ["/dir01/entrypoint.sh"]
